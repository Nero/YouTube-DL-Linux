clear
echo "                                YouTube-DL by Nero"
echo "                               ********************"
echo
echo "Updates prüfen und downloaden..."
.res/youtube-dl -U
clear
echo "                                YouTube-DL by Nero"
echo "                               ********************"
echo 
echo "1 = Audio (mp3)"
echo "2 = Video (mp4)"
echo "3 = Video (bestmögliche Qualität)"
echo "4 = Playlist (mp3)"
echo "5 = Playlist (mp4)"
echo "6 = Audio herunterladen und öffnen/in Playlist einreihen"
echo "0 = Verlassen"
echo
echo "Gib den jeweiligen Modus doppelt ein, um zu versuchen, den Netzwerk-Verkehr"
echo "durch das TOR-Netzwerk laufen zu lassen. Beispiel: '22'"
echo
read -p "Wahl: " modus
clear
echo "                             YouTube-DL by Nero - [$modus]"
echo "                            **************************"
echo

#mp3
if [ $modus == "1" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist -x --audio-format mp3 --audio-quality 0 -o "Downloads/%(title)s.%(ext)s" --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n"{}'
    echo
    done

#mp4
elif [ $modus == "2" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist -o "Downloads/%(title)s.%(ext)s" --recode-video mp4 --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n{}"'
    echo
    done

#best
elif [ $modus == "3" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist -o "Downloads/%(title)s.%(ext)s" --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n{}"'
    echo
    done

#playlist_mp3
elif [ $modus == "4" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl $tag -q -R 5 -x --audio-format mp3 --audio-quality 0 --yes-playlist -o "Downloads/%(title)s.%(ext)s"
    echo Playlist wurde heruntergeladen!
    echo
    done

#playlist_mp4
elif [ $modus == "5" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl $tag -q -R 5 --yes-playlist -o "Downloads/%(title)s.%(ext)s" --recode-video mp4
    echo Playlist wurde heruntergeladen!
    echo
    done

#clementine
elif [ $modus == "6" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist -x --audio-format mp3 --audio-quality 0 -o "Downloads/%(title)s.%(ext)s" --exec 'clementine {} && echo "\nFolgender Titel wurde heruntergeladen und eingereiht:\n"{}'
    echo
    done

#################################### TOR ####################################

#mp3 (Tor)
elif [ $modus == "11" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist -x --audio-format mp3 --audio-quality 0 --proxy socks5://ultimatecores.nsupdate.info:9050/ -o "Downloads/%(title)s.%(ext)s" --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n"{}'
    echo
    done

#mp4 (Tor)
elif [ $modus == "22" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist --proxy socks5://ultimatecores.nsupdate.info:9050/ -o "Downloads/%(title)s.%(ext)s" --recode-video mp4 --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n{}"'
    echo
    done

#best (Tor)
elif [ $modus == "33" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist --proxy socks5://ultimatecores.nsupdate.info:9050/ -o "Downloads/%(title)s.%(ext)s" --exec 'echo "\nFolgender Titel wurde heruntergeladen:\n{}"'
    echo
    done

#playlist_mp3 (Tor)
elif [ $modus == "44" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl $tag -q -R 5 -x --audio-format mp3 --audio-quality 0 --proxy socks5://ultimatecores.nsupdate.info:9050/ --yes-playlist -o "Downloads/%(title)s.%(ext)s"
    echo Playlist wurde heruntergeladen!
    echo
    done

#playlist_mp4 (Tor)
elif [ $modus == "55" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag: " tag
    if [ "$tag" == "0" ]
    then 
    ./YouTube-DL.sh
    fi
    .res/youtube-dl $tag -q -R 5 --yes-playlist --proxy socks5://ultimatecores.nsupdate.info:9050/ -o "Downloads/%(title)s.%(ext)s" --recode-video mp4
    echo Playlist wurde heruntergeladen!
    echo
    done

#clementine (Tor)
elif [ $modus == "66" ]
then
    while [ 1 == 1 ]
    do
    read -p "Link / Tag / Titel: " tag
    if [ "$tag" == "0" ]
    then
    ./YouTube-DL.sh
    fi
    .res/youtube-dl --default-search "ytsearch" "$tag" -q -R 5 --no-playlist -x --audio-format mp3 --audio-quality 0 --proxy socks5://ultimatecores.nsupdate.info:9050/ -o "Downloads/%(title)s.%(ext)s" --exec 'xdg-open {} && echo "\nFolgender Titel wurde heruntergeladen und eingereiht:\n"{}'
    echo
    done

#Verlassen
elif [ $modus == "0" ]
then
    exit

#Kein Modus
else
echo "Falscher Modus!"
echo "Neustart..."
sleep 2 && ./YouTube-DL.sh
fi
